﻿using System;
using System.Collections.Generic;
using System.Text;

namespace In_MemoryFileSystem
{
    public class File : Entry
    {
        public string Content { get; set; }
        public int Size { get; private set; }
        public File (string name, Directory parent, string content)
            :base(name)
        {
            if (FreeSpace - this.Content.Length*2 < 0)
            {
                throw new ArgumentException("There is not enough disk space!");
            }
            this.Size = this.Content.Length * 2;
            FreeSpace -= this.Size;
        }
        public override int CalculateSize()
        {
            // 1 char = 2 bytes
            return this.Content.Length*2;
        }
        public override string Open()
        {
            this.LastAccessedOn = DateTime.Now;
            return this.Content;
        }
        public void Write(string content)
        {
            FreeSpace += this.CalculateSize();
            this.Size = this.CalculateSize();
            if(FreeSpace - content.Length*2 <= 0)
            {
                throw new ArgumentException("There is NOT enough disk space!");
            }
            this.Content = content;
        }
    }
}
