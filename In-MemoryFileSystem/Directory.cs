﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace In_MemoryFileSystem
{
	public class CopyPasteManager
	{
		private Entry copiedEntry;
		public void Copy(Entry entry)
		{
			this.copiedEntry = entry;
		}

		public void Paste(Directory host)
		{
			host.Add(this.copiedEntry);
		}
	}
	public class Directory : Entry
	{
		private Entry copiedEntry;
		public List<Entry> Items = new List<Entry>();
		private readonly CopyPasteManager copyPasteManager;

		public Directory(string name, CopyPasteManager copyPasteManager)
			: base(name)
		{
			this.copyPasteManager = copyPasteManager;
		}
		public override int CalculateSize()
		{
			int size = 0;
			foreach (var item in this.Items)
			{
				size += item.CalculateSize();
			}
			return size;
		}
		public override string Open()
		{
			// OPENS THE DIRECTORY AND SHOWS ITS CONTENTS
			StringBuilder sb = new StringBuilder();
			foreach (var entry in this.Items)
			{
				sb.AppendLine(entry.Name);
			}
			this.LastAccessedOn = DateTime.Now;
			return sb.ToString();
		}
		public int Count
		{
			get
			{
				int count = 0;

				foreach (var item in this.Items)
				{
					if (item is Directory)
					{
						count += ((Directory)item).Count;
					}

					count++;
				}

				return count;
			}
		}

		public bool Delete(Entry entry)
		{
			//todo
			if (entry is File)
			{
				FreeSpace += ((File)entry).Size;
			}
			return this.Items.Remove(entry);
		}
		public void Add(Entry entry)
		{
			// todo: cannot add items with the same name
			entry.Parent = this;
			this.Items.Add(entry);
		}
		public void SortByName()
		{
			this.Items = this.Items.OrderBy(d => d.Name).ToList();
		}
		public void SortByDateOfCreation()
		{
			this.Items = this.Items.OrderBy(d => d.CreatedOn).ToList();
		}
		public void Copy(Entry entry)
		{
			this.copyPasteManager.Copy(entry);
		}
		public void Paste()
		{
			this.copyPasteManager.Paste(this);
		}
		public bool FindWithBFS(string name)
		{
			Console.WriteLine(this.Name);
			if (this.Name == name)
				return true;

			var queue = new Queue<Entry>();

			foreach (var item in this.Items)
			{
				queue.Enqueue(item);
			}

			while (queue.Count > 0)
			{
				var current = queue.Dequeue();
				Console.WriteLine(current.Name);
				if (current.Name == name)
				{
					return true;
				}
				if (current is Directory)
				{
					foreach (var item in ((Directory)current).Items)
					{
						queue.Enqueue(item);
					}
				}
			}
			return false;
		}
		public bool ContainsWithDFS(string name)
		{
			Console.WriteLine(this.Name);
			if (this.Name == name)
				return true;

			var stack = new Stack<Entry>();

			foreach (var item in this.Items)
			{
				stack.Push(item);
			}

			while (stack.Count > 0)
			{
				var current = stack.Pop();
				Console.WriteLine(current.Name);
				if (current.Name == name)
				{
					return true;
				}
				if (current is Directory)
				{
					foreach (var item in ((Directory)current).Items)
					{
						stack.Push(item);
					}
				}
			}
			return false;
		}
		public Entry BreadthFirstSearch(string name)
		{
			List<Entry> directoriesOnCurrentLevel = this.Items;
			List<Entry> directoriesSavedForNextLevel = new List<Entry>();
			while (directoriesSavedForNextLevel.Count != 0)
			{
				directoriesSavedForNextLevel.Clear();
				foreach (var entry in directoriesOnCurrentLevel)
				{
					if (entry.Name == name)
					{
						return entry;
					}
					else if (entry is Directory)
					{
						directoriesSavedForNextLevel.Add(entry);
					}
				}
				directoriesOnCurrentLevel = directoriesSavedForNextLevel;
			}
			return null;
		}
		public Entry DepthFirstSearch(string name)
		{

			Stack<Directory> inCheck = new Stack<Directory>();
			List<Directory> checkedDirectories = new List<Directory>();
			foreach (var entry in this.Items)
			{
				if (entry.Name == name)
				{
					return entry;
				}
				else if (entry is Directory)
				{
					inCheck.Push(entry as Directory);
				}
			}

			Directory currentDirectory = inCheck.Pop();
			while (inCheck.Count != 0)
			{
				foreach (var entry in currentDirectory.Items)
				{
					if (entry.Name == name)
					{
						return entry;
					}
					else if (entry is Directory && !checkedDirectories.Contains(entry))
					{
						inCheck.Push(entry as Directory);
					}
				}
				checkedDirectories.Add(currentDirectory);
				currentDirectory = inCheck.Pop();
			}
			return null;
		}
	}
}
