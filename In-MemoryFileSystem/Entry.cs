﻿using System;
using System.Collections.Generic;
using System.Text;

namespace In_MemoryFileSystem
{
    public abstract class Entry
    {
        protected static int FreeSpace = 1024;

        public static Entry Copied;

        internal DateTime CreatedOn { get; private set; }
        internal DateTime LastUpdatedOn { get; set; }
        internal DateTime LastAccessedOn { get; set; }
        internal string Name { get; private set; }
        public Entry(string name)
        {
            this.Name = name;
            this.CreatedOn = DateTime.Now;
            this.LastUpdatedOn = DateTime.Now;
            this.LastAccessedOn = DateTime.Now;
        }
        public Directory Parent
        {
            get; set;
        }
        public bool Delete()
        {
            if (this.Parent == null)
                return false;

            return this.Parent.Delete(this);
        }
        public void Rename(string name)
        {
            this.Name = name; // todo check parent for existing
            this.LastUpdatedOn = DateTime.Now;
        }
        public string FullPath
        {
            get
            {
                var backtrack = new Stack<string>();
                var current = this;
                while(current != null)
                {
                    backtrack.Push(current.Name);
                    current = current.Parent;
                }
                return string.Join(" > ", backtrack);
            }
        }
        public abstract int CalculateSize();
        public abstract string Open();
        public override bool Equals(object obj)
        {
            Entry other = obj as Entry;
            return this.Name == other?.Name; // todo +path
        }

    }
}
