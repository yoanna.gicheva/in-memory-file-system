﻿using System;

namespace In_MemoryFileSystem
{
	class Program
	{
		static void Main(string[] args)
		{
			/*  
             *            [root]
			 *          /        \
             *         /          \
             *     [2.1] -- x      [2.2]
             *      / \           /
             *  [3.1] [3.2]     [3.3]
			 *              
             *              
             */

			var copyPasteManager = new CopyPasteManager();
			var root = new Directory("root", copyPasteManager);

			var dir21 = new Directory("2.1", copyPasteManager);
			var dir22 = new Directory("2.2", copyPasteManager);

			root.Add(dir21);

			dir21.Add(new Directory("3.1", copyPasteManager));
			dir21.Add(new Directory("3.2", copyPasteManager));
			var target = new Directory("x", copyPasteManager);
			dir21.Add(target);

			root.Add(dir22);
			var dir33 = new Directory("3.3", copyPasteManager);
			dir22.Add(dir33);
			//var target = new Directory("x", dir33);
			//dir33.Add(target);

			//root.FindWithBFS("x");

			Console.WriteLine(target.FullPath);
		}
	}
}
